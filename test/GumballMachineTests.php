<?php
require 'GumballMachine.php';

class GumballMachineTests extends PHPUnit_Framework_TestCase{
	public $gumballMachineInstance;
	
	public function SetUp(){
		$this->gumballMachineInstance = new GumballMachine();
	}

	public function testIfWheelworks(){
		$this->gumballMachineInstance->setGumballs(100);
		$this->gumballMachineInstance->turnWheel();
		$this->assertEquals(99, $this->gumballMachineInstance->getGumballs());
	}
}
?>
